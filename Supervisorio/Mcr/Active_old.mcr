!===== Macro File created  07/19/00 ===============================
! Macros are lists of commands, with one command per line
! See Help or the manual for a list of commands and their parameters
!============================================================
DisplayServerOn


DataLogOn PAINT_MIX_BASE

DataLogOn PAINT_MIX_PRIMER

DataLogOn PAINT_MIX_VERNIZ_SOL

Display Trend_PM_01 /ZA
Trend\OldestTime = System\DateAndTimeInteger

Display Trend_PM_02 /ZA
Trend\OldestTime = System\DateAndTimeInteger

Display Trend_PM_03 /ZA
Trend\OldestTime = System\DateAndTimeInteger